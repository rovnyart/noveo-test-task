import { parse } from 'querystring';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { ConnectedRouter, routerMiddleware, LOCATION_CHANGE } from 'react-router-redux';
import { Route } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import createReducer from './reducers';
import App from './App';
import { loadItems } from './actions';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'noty/lib/noty.css';
import 'noty/lib/themes/mint.css';
import './styles.css';


function addLocationQuery(historyObject) {
  Object.assign(historyObject.location, { query: parse(historyObject.location.search.slice(1)) });
}

const browserHistory = createHistory();
addLocationQuery(browserHistory);
browserHistory.listen(() => addLocationQuery(browserHistory));

const pathMiddleware = (store) => (next) => (action) => {
  if (action.type === LOCATION_CHANGE) {
    const { app: { token } } = store.getState();
    const browsePath = /browse\/(.*)/.exec(action.payload.pathname || '');
    if (token && browsePath) return next(loadItems(browsePath[1]));
  }
  return next(action);
};

const plugins = [
  pathMiddleware,
  thunk,
  routerMiddleware(browserHistory),
];

const store = createStore(createReducer(), composeWithDevTools(applyMiddleware(...plugins)));

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={browserHistory}>
      <Route path="/" component={App} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
