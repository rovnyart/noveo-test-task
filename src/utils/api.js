import axios from 'axios';

const ax = (token) => axios.create({
  baseURL: 'https://cloud-api.yandex.net:443/v1',
  headers: {
    'Content-Type': 'application/json',
    Authorization: `OAuth ${token}`,
  },
});

export default ax;
