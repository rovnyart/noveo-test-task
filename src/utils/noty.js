import Noty from 'noty';

export const showError = (text) => new Noty({ text, type: 'error', timeout: 10000 }).show();
