export default function helper(map, initialState = {}) {
  return (state = initialState, action) => {
    if ((action.type) === '[APP]:CLEAR_STATE') return initialState;

    if (map[action.type]) {
      return map[action.type](state, action);
    }

    return state;
  };
}
