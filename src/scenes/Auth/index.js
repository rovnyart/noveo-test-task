import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Button } from 'reactstrap';

import logo from './images/YandexDisk.png';

const CLIENT_ID = '88b94aa6f88540b097ee57209b7b58e0';
const AUTH_PAGE = 'https://oauth.yandex.ru/authorize?response_type=token';

@withStyles((theme) => ({
  root: {
    width: theme.spacing.unit * 60,
    height: theme.spacing.unit * 50,
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.common.white,
    borderRadius: 6,
  },
}))
export default class Auth extends React.Component {
  handleAuthClick = () => document.location.replace(`${AUTH_PAGE}&client_id=${CLIENT_ID}`);

  render() {
    const { classes } = this.props;
    return (
      <Grid
        className={classes.root} container direction="column"
        spacing={40} justify="center" alignItems="center"
      >
        <Grid item>
          <img src={logo} alt="logo" width={128} />
        </Grid>
        <Grid item><Button color="primary" onClick={this.handleAuthClick}>Авторизация</Button></Grid>
        <Grid item><h6><strong>Авторизуйтесь для доступа к Яндекс.Диску</strong></h6></Grid>
      </Grid>
    );
  }
}
