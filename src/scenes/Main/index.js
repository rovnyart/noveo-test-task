import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import * as actions from '../../actions';

import Header from './components/Header';
import Explorer from './components/Explorer';
import Navigation from './components/Navigation';

@withStyles((theme) => ({
  root: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.common.white,
    borderRadius: 6,
    height: '100%',
    width: '100%',
    overflow: 'hidden',
  },
}))
@connect(({ app: { token, items, diskInfo } }) => ({ diskInfo, token, items }), actions)
export default class Index extends React.Component {
  componentDidMount() {
    const { loadDiskInfo, token } = this.props;
    if (token) loadDiskInfo();
  }

  handleFolderOpen = (path) => () => {
    const { match: { url }, history: { push } } = this.props;
    push(`${url}/${path}`);
  };

  render() {
    const { classes, token, items, diskInfo, history, location: { pathname } } = this.props;

    if (!token) return <Redirect to="/" />;

    return (
      <Paper className={classes.root}>
        <Grid container spacing={16} direction="column">
          <Grid item><Header diskInfo={diskInfo} /></Grid>
          <Grid item><Navigation history={history} dir={pathname.split('/').pop()} /></Grid>
          <Grid item><Explorer items={items} open={this.handleFolderOpen} /></Grid>
        </Grid>
      </Paper>
    );
  }
}
