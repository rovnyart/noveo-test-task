import React from 'react';
import Grid from '@material-ui/core/Grid';

const getMegabytesLabel = (value) => (value ? `${(value / 1024 / 1024).toFixed(2)} Мб` : 0);

export default function Header({ diskInfo = {} }) {
  const { used_space: usedSpace, total_space: totalSpace, user: { display_name: displayName } = {} } = diskInfo;
  return (
    <Grid container spacing={24} justify="space-between">
      <Grid item>
        <h2>Файлы</h2>
      </Grid>
      <Grid item>
        <Grid container spacing={16}>
          <Grid item><h6>{`Пользователь: ${displayName || ''}`}</h6></Grid>
          <Grid item><h6>{`Всего доступно: ${getMegabytesLabel(totalSpace)}`}</h6></Grid>
          <Grid item><h6>{`Использовано: ${getMegabytesLabel(usedSpace)}`}</h6></Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
