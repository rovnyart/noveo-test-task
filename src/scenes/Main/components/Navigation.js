import React from 'react';
import ButtonBase from '@material-ui/core/ButtonBase';
import Grid from '@material-ui/core/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withStyles } from '@material-ui/core/styles';

function Navigation({ history: { goForward, goBack }, dir, classes }) {
  return (
    <Grid container spacing={24} alignItems="center">
      <Grid item>
        <ButtonBase onClick={goBack} className={classes.button}>
          <FontAwesomeIcon size="2x" color="grey" icon="arrow-left" />
        </ButtonBase>
      </Grid>
      <Grid item>
        <ButtonBase onClick={goForward} className={classes.button}>
          <FontAwesomeIcon size="2x" color="grey" icon="arrow-right" />
        </ButtonBase>
      </Grid>
      <Grid item>
        <h6><strong>{dir}</strong></h6>
      </Grid>
    </Grid>
  );
}

export default withStyles((theme) => ({
  button: {
    padding: theme.spacing.unit,
    '&:hover': { backgroundColor: theme.palette.grey[100] },
    '&:focus': { outline: 'none' },
  },
}))(Navigation);
