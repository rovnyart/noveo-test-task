import React from 'react';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';


function Item({ name, icon, title, classes, action }) {
  return (
    <Grid
      container direction="column" className={classNames(classes.root, { [classes.pointer]: action })}
      alignItems="center" role="button" onClick={action || undefined}
      title={title}
      justify="center"
    >
      <Grid item><img src={icon} alt="item" width={64} /></Grid>
      <Grid item className={classes.nameContainer}><div className={classes.name}>{name}</div></Grid>
    </Grid>
  );
}


export default withStyles((theme) => ({
  root: {
    width: theme.spacing.unit * 10,
    height: theme.spacing.unit * 13,
    '&:hover': { backgroundColor: theme.palette.grey[200] },
  },
  pointer: { '&:hover': { cursor: 'pointer' } },
  name: { fontSize: '0.9em', fontWeight: 400, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' },
  nameContainer: { maxWidth: theme.spacing.unit * 10 },
}))(Item);
