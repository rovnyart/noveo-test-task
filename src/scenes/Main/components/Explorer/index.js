import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Item from './components/Item';
import folderIcon from './images/folder-icon.png';
import fileIcon from './images/file-icon.png';

function Explorer({ items, open, classes }) {
  return items.length > 0 ? (
    <Grid className={classes.root} container spacing={40}>
      {items.map(({ type, path, size, name }, index) => {
        const isFolder = type === 'dir';
        const props = {
          icon: isFolder ? folderIcon : fileIcon,
          action: isFolder ? open(path) : undefined,
          title: isFolder ? name : `${name} ${(size / 1024).toFixed(2)} Кб`,
          name,
        };
        return <Item key={String(index)} {...props} />;
      })}
    </Grid>
  ) : (
    <h6>Эта папка пуста</h6>
  );
}

export default withStyles((theme) => ({
  root: { padding: theme.spacing.unit * 2 },
}))(Explorer);
