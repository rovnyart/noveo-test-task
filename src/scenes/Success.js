import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import * as actions from '../actions';

@withStyles((theme) => ({
  root: {
    width: theme.spacing.unit * 50,
    height: theme.spacing.unit * 50,
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.common.white,
    borderRadius: 6,
  },
}))
@connect(undefined, actions)
export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = { redirect: false };
  }

  componentDidMount() {
    const { location, setToken } = this.props;
    const token = /access_token=([^&]+)/.exec(location.hash)[1];
    setToken(token);
    setTimeout(() => this.setState({ redirect: true }), 2000);
  }

  render() {
    const { redirect } = this.state;
    const { classes } = this.props;
    return redirect ? <Redirect to="/browse/disk:/" /> : (
      <Grid
        container direction="column" justify="center"
        alignItems="center" spacing={40} className={classes.root}
      >
        <Grid item>
          <FontAwesomeIcon size="10x" icon="check-circle" color="#33ff33" />
        </Grid>
        <Grid item><h5>Успешная авторизация Yandex API</h5></Grid>
      </Grid>
    );
  }
}
