import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faArrowLeft, faArrowRight, faCheckCircle } from '@fortawesome/free-solid-svg-icons';


import * as actions from './actions';
import Auth from './scenes/Auth';
import Main from './scenes/Main';
import Success from './scenes/Success';

library.add(faArrowLeft, faArrowRight, faCheckCircle);

@withStyles((theme) => ({
  root: {
    position: 'relative',
    display: 'flex',
    width: '100vw',
    height: '100vh',
    overflow: 'auto',
  },
  content: {
    padding: theme.spacing.unit * 5,
    flexGrow: 1,
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  authButton: { width: theme.spacing.unit * 15 },
}))
@connect((state) => ({ token: state.app.token }), actions)
export default class App extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <main className={classes.content}>
          <Switch>
            <Route path="/auth" component={Auth} />
            <Route path="/success" component={Success} />
            <Route path="/browse" component={Main} />
            <Route render={() => <Redirect to="/auth" />} />
          </Switch>
        </main>
      </div>
    );
  }
}
