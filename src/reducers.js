import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import helper from './utils/store';

const INITIAL_STATE = {
  token: null,
  diskInfo: {},
  items: [],
};

const app = helper({
  '[APP]:SET_TOKEN': (state, { token }) => ({ ...state, token }),
  '[API]:LOAD_DISK_INFO': (state, { diskInfo }) => ({ ...state, diskInfo }),
  '[API]:LOAD_ITEMS': (state, { items }) => ({ ...state, items }),
}, INITIAL_STATE);

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer() {
  return combineReducers({
    route: routerReducer,
    app,
  });
}
