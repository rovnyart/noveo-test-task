import api from './utils/api';
import { showError } from './utils/noty';

export function setToken(token) {
  return async (dispatch) => {
    dispatch({ type: '[APP]:SET_TOKEN', token });
  };
}

export function loadDiskInfo() {
  return async (dispatch, getState) => {
    const { token } = getState().app;
    try {
      const { data: diskInfo } = await api(token).get('/disk/');
      dispatch({ type: '[API]:LOAD_DISK_INFO', diskInfo });
    } catch (error) {
      showError(error.message);
    }
  };
}

export function loadItems(path) {
  return async (dispatch, getState) => {
    const { token } = getState().app;
    try {
      const { data } = await api(token).get(`/disk/resources?path=${path}`);
      const { _embedded: { items } } = data;
      dispatch({ type: '[API]:LOAD_ITEMS', items });
    } catch (error) {
      showError(error.message);
    }
  };
}
